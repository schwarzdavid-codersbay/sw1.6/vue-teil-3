import {defineStore} from "pinia";

const usePersonsStore = defineStore('persons', {
    state: () => ({
        persons: []
    })
})

export {usePersonsStore}


/*function state() {
    return 1;
}
const state = () => {
    return {
        persons: []
    }
};

const state = () => ({
    persons: []
})*/
