function findFooInBar(bar) {
    return bar['foo']
}

function foo() {}

function bar() {}

function printX() {}

//export default findFooInBar; // 🤡🤡🤡

export {
    findFooInBar,
    foo,
    bar,
    printX
}
