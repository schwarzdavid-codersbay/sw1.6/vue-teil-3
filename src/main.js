import App from './App.vue'
import {createApp} from "vue";
import {router} from "@/router";  // <- router-Instanz wird von router/index.js geladen
import {createPinia} from "pinia"; // <- pinia wird importiert

import 'bootstrap/dist/css/bootstrap.css'

createApp(App)
    .use(router) // <- router wird zur vue app hinzugefügt
    .use(createPinia()) // <- pinia wird zur vue app hinzugefügt
    .mount('#app')
